"""
 Copyright (c) 2012, Nimar S. Arora <nimar.arora@gmail.com>
 All rights reserved.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
     * Redistributions of source code must retain the above copyright
       notice, this list of conditions and the following disclaimer.
     * Redistributions in binary form must reproduce the above copyright
       notice, this list of conditions and the following disclaimer in the
       documentation and/or other materials provided with the distribution.
     * Neither the name of the copyright holders nor the
       names of its contributors may be used to endorse or promote products
       derived from this software without specific prior written permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 THE COPYRIGHT HOLDERS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.
"""
import os
from distutils.cmd import Command
from setuptools.command import build_ext
from setuptools import setup, Extension
from setuptools.extension import Library
from glob import glob

NAME = "pyblogc"
VERSION = "1.0.3"

# ==================================
# This section of code is to force setuptools to create static libraries on
# NT platform for the Library extensions
# ==================================
build_ext.libtype = 'static'

# this function is copied from setuptools
def link_shared_object(self, objects, output_libname, output_dir=None,
                       libraries=None, library_dirs=None,
                       runtime_library_dirs=None,
                       export_symbols=None, debug=0, extra_preargs=None,
                       extra_postargs=None, build_temp=None, target_lang=None
                       ):
  # XXX we need to either disallow these attrs on Library instances,
  #     or warn/abort here if set, or something...
  # libraries=None, library_dirs=None, runtime_library_dirs=None,
  # export_symbols=None, extra_preargs=None, extra_postargs=None,
  # build_temp=None
  assert output_dir is None   # distutils build_ext doesn't pass this
  output_dir,filename = os.path.split(output_libname)
  basename, ext = os.path.splitext(filename)
  if self.library_filename("x").startswith('lib'):
    # strip 'lib' prefix; this is kludgy if some platform uses
    # a different prefix
    basename = basename[3:]
  self.create_static_lib(objects, basename, output_dir, debug, target_lang)
  
build_ext.link_shared_object = link_shared_object
# ==================================

pyruntime = Extension (
  name = "pyblogc.libraries.runtime",
  sources = glob("src/extmod/runtime.c")+glob("src/lib/*.c"),
  depends = glob("src/extmod/runtime.h")+glob("src/include/*.h"),
  include_dirs = ["src/include"],
  )

libblogc = Library (
  name = "pyblogc.libraries.blogc",
  sources = glob("src/lib/*.c"),
  depends = glob("src/include/*.h") + glob("src/lib/*.h"),
  include_dirs = ["src/include"],
  )

dist = setup (
  name = NAME,
  version = VERSION,
  obsoletes = ["%s(<%s)" % (NAME, VERSION)],
  package_dir = {"" : "src"},
  packages = ["pyblogc"],

  # using the "headers" options puts the header files in strange locations
  # and install_headers is not always called, safer to use as data files
  #package_data = {"pyblogc" : ["headers/*.h", "objects/*.h", "objects/*.c"]},
  data_files = [("pyblogc/headers", glob("src/include/*.h"))],
  entry_points = {"console_scripts":
                  ["blogc = pyblogc.main:main"]},
  ext_modules = [pyruntime, libblogc],
  zip_safe = False,
  
  # TODO: documentation

  # metadata for upload to PyPI
  author = "Nimar S. Arora",
  author_email = "nimar.arora@gmail.com",
  description = "Python implementation of Bayesian Logic (BLOG) Compiler",
  license = "BSD",
  keywords = "MCMC Open Universe Probability Models Bayesian Logic",
  url = "https://pyblogc.googlecode.com",
  long_description =
  """
  Open Universe Probability Models (OUPMs) describe situations with an unknown
  number of objects with identity and relational uncertainty. Bayesian
  Logic (BLOG) is a declarative language for OUPMs within the framework of
  first-order logic.

  This package implments a compiler for models written in the BLOG
  language.  The inference for a model is compiled into C++ code
  that can be further compiled into executable programs. The resulting
  program produces the samples from the posterior distribution and it can
  optionally take the evidence as input (or the evidence could be supplied
  at compile time).
  """

  # TODO: classifiers
  
  )

