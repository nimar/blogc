"""
 Copyright (c) 2012, Nimar S. Arora <nimar.arora@gmail.com>
 All rights reserved.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
     * Redistributions of source code must retain the above copyright
       notice, this list of conditions and the following disclaimer.
     * Redistributions in binary form must reproduce the above copyright
       notice, this list of conditions and the following disclaimer in the
       documentation and/or other materials provided with the distribution.
     * Neither the name of the copyright holders nor the
       names of its contributors may be used to endorse or promote products
       derived from this software without specific prior written permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 THE COPYRIGHT HOLDERS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.
"""
import unittest
import sys
import subprocess
import os

WILDCARD = '*'
APPROXIMATE = '~'

DIRECTORIES = ["examples/negative", "examples/bayesnet"]

def test_all():
  totsuc, toterr = 0, 0

  for dirname in DIRECTORIES:
    suc, err = discover_tests(dirname)
    totsuc += suc
    toterr += err
  
  print "%d succeeded; %d failed" % (totsuc, toterr)

def discover_tests(dirname):
  print dirname, ":"
  # save the current directory
  cwd = os.getcwd()
  os.chdir(dirname)

  succnt, errcnt = 0, 0
  
  for filename in os.listdir("."):
    if os.path.splitext(filename)[1] == ".blog":
      lines = file(filename).readlines()
      
      for idx, line in enumerate(lines):
        line = line.rstrip()
        if line.startswith("/*+PYBLOGC_TEST+"):
          nextidx = idx+1
          break
      else:
        continue                        # next file

      commands = []
      
      for idx, line in enumerate(lines[nextidx:]):
        line = line.rstrip()
        if line.startswith("+OUTPUT+"):
          nextidx += idx + 1
          break
        commands.append(line)
      else:
        continue                        # next file

      output = []
      for idx, line in enumerate(lines[nextidx:]):
        line = line.rstrip()
        if line.startswith("+ERROR+"):
          nextidx += idx+1
          break
        output.append(line)
      else:
        continue                        # next file

      error = []
      for idx, line in enumerate(lines[nextidx:]):
        line = line.rstrip()
        if line.startswith("+PYBLOGC_TEST+*/"):
          nextidx += idx+1
          break
        error.append(line)
      else:
        continue                        # next file

      # now we will run this test
      fullout, fullerr = [], []
      for command in commands:
        _, out, err = run_os_command(command)
        for line in out.split(os.linesep):
          fullout.append(line.rstrip())
        for line in err.split(os.linesep):
          fullerr.append(line.rstrip())

      if verify_output(output, fullout) and verify_output(error, fullerr):
        print "  ", filename, "... ok"
        succnt += 1
      else:
        print "  ", filename, "... err"
        errcnt += 1

  # restore directory
  os.chdir(cwd)

  return succnt, errcnt

def verify_output(pattern, output):
  failure = 0
  
  if len(pattern) != len(output):
    failure = 1
    print "Pattern has %d lines while output has %d" % (len(pattern),
                                                        len(output))

  unmatched_output = 0

  patlnum = 0
  outlnum = 0

  while patlnum < len(pattern) and outlnum < len(output):
    patline = pattern[patlnum]
    outline = output[outlnum]

    if not wildcard_match(patline, outline):
      failure = 1

      if not unmatched_output:
        print "Unmatched output:"
        unmatched_output = 1
      
      print outline
      
    else:
      patlnum += 1
      
    outlnum += 1

  if outlnum < len(output):
    if not unmatched_output:
      print "Unmatched output:"
    for ol in output[outlnum:]:
      print ol

  if patlnum < len(pattern):
    print "Unmatched pattern:"
    for pl in pattern[patlnum:]:
      print pl

  if not failure:
    return True

NUMERIC_CHARS = set(["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", ".",
                     "e", "E", "+", "-"])

# find the position of the first non-numeric character or -1
def find_non_num(string, startpos=0):
  for pos in range(startpos, len(string)):
    if string[pos] not in NUMERIC_CHARS:
      return pos

  return -1
  
def wildcard_match(patline, outline):
  patline = patline.rstrip()
  outline = outline.rstrip()  
  if outline == patline:
    return 1

  outpos = 0
  patpos = 0
  while patpos < len(patline):
    # a wildcard at the end of the line matches the rest of the output
    # it even matches if the output is exhausted
    if patline[patpos] == WILDCARD and patpos == len(patline)-1:
      return 1

    # if we have exhausted the output then the pattern is not being matched
    if outpos >= len(outline):
      return 0

    # if we have an approximate number then we should get both the numbers
    # and check for a match
    if patline[patpos] == APPROXIMATE:
      patpos += 1

      patpos_end = find_non_num(patline, patpos)
      if patpos_end == -1:
        pat_num = float(patline[patpos:])
      else:
        pat_num = float(patline[patpos:patpos_end])
      
      outpos_end = find_non_num(outline, outpos)
      if outpos_end == -1:
        out_num = float(outline[outpos:])
      else:
        out_num = float(outline[outpos:outpos_end])

      # if the two numbers differ by more than 20% then there is no match
      if (out_num != pat_num and
          abs((out_num - pat_num)/(out_num/2 + pat_num/2)) > .2):
        return 0
      
      if patpos_end == -1:
        patpos = len(patline)
      else:
        patpos = patpos_end
      
      if outpos_end == -1:
        outpos = len(outline)
      else:
        outpos = outpos_end
      
      continue

    # if the next character matches exactly, we need to move forward trivially
    if patline[patpos] == outline[outpos]:
      outpos += 1
      patpos += 1
      continue

    # if the next pattern char is a wildcard we need to skip over the input
    # till it matches the pattern char after the wildcard
    if patline[patpos] == WILDCARD:
      patpos += 1
      while outpos < len(outline) and patline[patpos] != outline[outpos]:
        outpos += 1

      if outpos < len(outline):
        patpos += 1
        outpos += 1
        continue
      else:
        return 0

    # otherwise, the pattern and the output char don't match, we have failed
    else:
      return 0

  # the pattern is exhaused, so the output better be exhausted as well
  if outpos < len(outline):
    return 0
  else:
    return 1
  
def run_os_command(command):
  """
  Execute a command and return the return value and generated outputs.
  Returns: errnum, stdout, stderr
  """
  proc = subprocess.Popen(command.split(), stdout=subprocess.PIPE,
                          stderr=subprocess.PIPE)
  out, err = proc.communicate()

  return proc.returncode, out.rstrip(), err.rstrip()

if __name__ == "__main__":
  test_all()


