/*
 Copyright (c) 2012, Nimar S. Arora <nimar.arora@gmail.com>
 All rights reserved.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
     * Redistributions of source code must retain the above copyright
       notice, this list of conditions and the following disclaimer.
     * Redistributions in binary form must reproduce the above copyright
       notice, this list of conditions and the following disclaimer in the
       documentation and/or other materials provided with the distribution.
     * Neither the name of the copyright holders nor the
       names of its contributors may be used to endorse or promote products
       derived from this software without specific prior written permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 THE COPYRIGHT HOLDERS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.
*/
#include <Python.h>

#include <blogc.h>
#include "runtime.h"

static PyObject * py_srand(PyObject * self, PyObject * args);
static PyObject * py_rand(PyObject * self, PyObject * args);

static PyMethodDef runtimeMethods[] = {
  {"srand", py_srand, METH_VARARGS,
   "srand(seed) : sets the random number generator seed"},
  
  {"rand", py_rand, METH_NOARGS,
   "rand() : returns a random integer"},
  
  {NULL, NULL}
};

void initruntime(void)
{
  PyObject * m;
  
  m = Py_InitModule3("runtime", runtimeMethods,
                     "Bayesian Logic Compiler runtime library");
}

static PyObject * py_srand(PyObject * self, PyObject * args)
{
  int seed;
  
  if (!PyArg_ParseTuple(args, "i", &seed))
    return NULL;
  
  blc_srand(seed);

  Py_INCREF(Py_None);
  
  return Py_None;
}

static PyObject * py_rand(PyObject * self, PyObject * args)
{
  int randnum = blc_rand();
  
  return Py_BuildValue("i", randnum);
}

