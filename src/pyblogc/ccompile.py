"""
 Copyright (c) 2012, Nimar S. Arora <nimar.arora@gmail.com>
 All rights reserved.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
     * Redistributions of source code must retain the above copyright
       notice, this list of conditions and the following disclaimer.
     * Redistributions in binary form must reproduce the above copyright
       notice, this list of conditions and the following disclaimer in the
       documentation and/or other materials provided with the distribution.
     * Neither the name of the copyright holders nor the
       names of its contributors may be used to endorse or promote products
       derived from this software without specific prior written permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 THE COPYRIGHT HOLDERS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.
"""
import os
import pkg_resources
from distutils import ccompiler

def ccompile_inference_code(context):
  hdrdir = pkg_resources.resource_filename(__name__, "headers")
  libdir = pkg_resources.resource_filename(__name__, "libraries")

  if context.verbose:
    print "Header Directory:", hdrdir
    print "Library Directory:", libdir
    print "Available compilers:",
    ccompiler.show_compilers()
    print "Generated File:", context.outfile
  
  # NOTE: set environment variable DISTUTILS_DEBUG if the actual error
  # message is desired
  ccompstr = ccompiler.get_default_compiler()
  if context.compiler:
    ccompstr = context.compiler
  if context.verbose:
    print "Compiler:", ccompstr
  ccomp = ccompiler.new_compiler(compiler=ccompstr, verbose=context.verbose)
  ccomp.add_include_dir(hdrdir)
  ccomp.add_library_dir(libdir)
  ccomp.add_library("blogc")
  ccomp.add_library("libstdc++")
  
  obj_fnames = ccomp.compile([context.outfile])

  os.remove(context.outfile)
  
  ccomp.link_executable(objects = obj_fnames,
                        output_progname = context.exefile)

  for obj_fname in obj_fnames:
    os.remove(obj_fname)
  
