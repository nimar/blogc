"""
 Copyright (c) 2012, Nimar S. Arora <nimar.arora@gmail.com>
 All rights reserved.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
     * Redistributions of source code must retain the above copyright
       notice, this list of conditions and the following disclaimer.
     * Redistributions in binary form must reproduce the above copyright
       notice, this list of conditions and the following disclaimer in the
       documentation and/or other materials provided with the distribution.
     * Neither the name of the copyright holders nor the
       names of its contributors may be used to endorse or promote products
       derived from this software without specific prior written permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 THE COPYRIGHT HOLDERS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.
"""
from tokenize import *

class Symbol:
  """
  name   - all names are tuples
  class_ - "type", "function", "object", "constant", "distribution"
  location "builtins" | (file-name, line-num, ch-pos)
  valtype - the type of value represented by this symbol

  class_ == "type":
    isfinite - has a finite number of objects

  class_ == "function":
    deptype - "random" | "nonrandom"
    deptree - the dependency statment
  
  """
  pass

def init_symbol_table(ctx):
  ctx.symtable = {}
  
  # add the builtin types
  new_builtin_type(ctx, ("Integer",), False)
  new_builtin_type(ctx, ("Real",), False)
  new_builtin_type(ctx, ("String",), False)
  new_builtin_type(ctx, ("Boolean",), True)

  new_builtin_symbol(ctx, ("true",), "object", ("Boolean",))
  new_builtin_symbol(ctx, ("false",), "object", ("Boolean",))
  
  new_builtin_symbol(ctx, ("null",), "object", None)

  
def new_builtin_type(ctx, name, isfinite):
  sym = new_builtin_symbol(ctx, name, "type", None)
  sym.isfinite = isfinite

def new_builtin_symbol(ctx, name, class_, valtype):
  return new_symbol(ctx, name, class_, valtype, None)

def new_symbol(ctx, name, class_, valtype, token):
  assert(type(name) is tuple)
  
  if name in ctx.symtable:
    token_print_error(ctx, token, "duplicate definition of '%s'" \
                      % format_symbol_name(name))
    print >> sys.stderr, "%s: previous definition" % \
          ":".join(ctx.symtable[name].location)
  symobj = Symbol()
  
  symobj.name = name
  symobj.class_ = class_
  if token is None:
    symobj.location = ("builtins",)
  else:
    symobj.location = (token[TOK_FNAMEPOS], str(token[TOK_LNUMPOS]),
                       str(token[TOK_CHPOS]))
  symobj.valtype = valtype
  
  ctx.symtable[name] = symobj
  
  return symobj

def print_symbol_table(ctx):
  print "Symbol Table:"
  print "------------"
  for symname, symobj in ctx.symtable.iteritems():
    if symobj.location != ("builtins",):
      print symobj.name, symobj.class_, symobj.valtype,
      if symobj.class_ == "function":
        print symobj.deptype
      else:
        print
  print "------------"

def format_symbol_name(symname):
  if len(symname) == 1:
    return symname[0]
  else:
    return symname[0] + "(" + ", ".join([format_symbol_name(s)
                                         for s in symname[1:]]) + ")"

