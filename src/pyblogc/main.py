"""
 Copyright (c) 2012, Nimar S. Arora <nimar.arora@gmail.com>
 All rights reserved.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
     * Redistributions of source code must retain the above copyright
       notice, this list of conditions and the following disclaimer.
     * Redistributions in binary form must reproduce the above copyright
       notice, this list of conditions and the following disclaimer in the
       documentation and/or other materials provided with the distribution.
     * Neither the name of the copyright holders nor the
       names of its contributors may be used to endorse or promote products
       derived from this software without specific prior written permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 THE COPYRIGHT HOLDERS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.
"""
import os
import sys
import pkg_resources
from optparse import OptionParser

from parse import parse_model_files
from generate import generate_inference_code
from ccompile import ccompile_inference_code
from execute import execute_inference_code

def main2():
  
  print_version()
  
  context = read_command_line()

  parse_model_files(context)

  generate_inference_code(context)
  
  if context.generateonly:
    return
  
  ccompile_inference_code(context)

  if context.compileonly:
    return

  execute_inference_code(context)

def print_version():
  dist = pkg_resources.require("pyblogc")[0]
  print "Bayesian Logic Compiler", dist.version


def read_command_line():
  parser = OptionParser(usage = "Usage: blogc [options] blog-files..")
  parser.add_option("-v", "--verbose", dest="verbose", default=False,
                    action="store_true",
                    help="print debugging output (False)")
  parser.add_option("--vv", dest="veryverbose", default=False,
                    action="store_true",
                    help="print a lot of debugging output (False)")
  parser.add_option("-g", "--generateonly", dest="generateonly", default=False,
                    action="store_true",
                    help="generate C++ code only, don't compile (False)")
  parser.add_option("-o", "--outfile", dest="outfile", default=None,
                    type="str", help="generate C++ code into <file>",
                    metavar="<file>")
  parser.add_option("-c", "--compileonly", dest="compileonly", default=False,
                    action="store_true",
                    help="compile BLOG model only, don't run inference (False)")
  parser.add_option("-e", "--exefile", dest="exefile", default=None,
                    type="str", help="compile executable into <file>",
                    metavar="<file>")
  parser.add_option("--compiler", dest="compiler", default=None,
                    type="str", help="use C++ compiler <comp>",
                    metavar="<comp>")
  parser.add_option("-n", "--numsamples", dest="numsamples", default=10000,
                    type="int", help="number of samples for inference",
                    metavar="<num>")
  (context, filelist) = parser.parse_args()
  
  if not len(filelist):
    print >> sys.stderr, "blogc: no input files"
    sys.exit(1)
  
  for filename in filelist:
    filext = os.path.splitext(filename)[1]
    if filext != ".blog":
      print >> sys.stderr, "blogc: unrecognized extension '%s'" % filext
      sys.exit(1)

  if not context.outfile:
    context.outfile = os.path.basename(os.path.splitext(filelist[0])[0])+".cpp"
    if context.verbose:
      print "read_command_line: outfile:", context.outfile
  
  if not context.exefile:
    context.exefile = os.path.basename(os.path.splitext(filelist[0])[0])
    if context.verbose:
      print "read_command_line: exefile:", context.exefile

  context.filelist = filelist

  if context.veryverbose:
    context.verbose=True
  
  return context


# invoke the debugger if main2 raises an exception other than exit()
def main():
  try:
    main2()
  except SystemExit:
    raise
  except:
    import pdb, traceback, sys
    traceback.print_exc(file=sys.stdout)
    pdb.post_mortem(sys.exc_traceback)
    raise

# comment the following line to invoke the debugged version
main = main2
    
