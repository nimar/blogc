"""
 Copyright (c) 2012, Nimar S. Arora <nimar.arora@gmail.com>
 All rights reserved.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
     * Redistributions of source code must retain the above copyright
       notice, this list of conditions and the following disclaimer.
     * Redistributions in binary form must reproduce the above copyright
       notice, this list of conditions and the following disclaimer in the
       documentation and/or other materials provided with the distribution.
     * Neither the name of the copyright holders nor the
       names of its contributors may be used to endorse or promote products
       derived from this software without specific prior written permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 THE COPYRIGHT HOLDERS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.
"""
import sys

LANGUAGEWORDS = ["type", "origin", "random", "nonrandom", "external", "if",
                 "else", "query", "obs", "guaranteed", "for", "external",
                 "elseif", "else", "then", "implicit", "null",
                 "List", "Set", "Matrix", "Vector", "Distribution"]

BOOLEANWORDS = ["true", "false"]

# generic tokens
TOK_TOKPOS, TOK_FNAMEPOS, TOK_LNUMPOS, TOK_CHPOS, TOK_VALPOS = range(5)

def unimp(token = None):
  if token is not None:
    print >>sys.stderr, "%s:%d:%d: error: unimplemented"\
          % (token[TOK_FNAMEPOS], token[TOK_LNUMPOS],
             token[TOK_CHPOS])
  else:
    print >> sys.stderr, "error: unimplemented"
  
  sys.exit(1)

def token_print_error(context, token, mesg):
  """
  Raise an exception after writing out the exact position of the token
  where the exception was encountered. Also increment context.errorcnt.
  """
  print >>sys.stderr, "%s:%d:%d: error: %s"\
        % (token[TOK_FNAMEPOS], token[TOK_LNUMPOS],
           token[TOK_CHPOS], mesg)
  context.errorcnt += 1

def tokenize(context, filename):
  """
  Convert a BLOG model file into a list of tokens.
  """
  try:
    fp = open(filename)
  except IOError, err:
    print >>sys.stderr, err
    raise SystemExit(1)
  
  chars = fp.read()
  tokens = []
  
  SPACECHARS = [' ', '\t', '\r', '\n']
  STRINGCHAR = '"'
  ESCAPESTRINGCHAR = '\\"'
  NUMBERCHARS = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
  IDENTIFIERCHARS = ['_',
                     'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k',
                     'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w',
                     'x', 'y', 'z',
                     'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K',
                     'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W',
                     'X', 'Y', 'Z']
  
  SINGLELINECOMMENTCHARS = '//'
  OPENMULTILINECOMMENTCHARS = '/*'
  CLOSEMULTILINECOMMENTCHARS = '*/'
  SYMBOLS2 = ['!=', '->', '>=', '<=']
  SYMBOLS1 = ['+', '-', '*', '/', '=', '!', '&', '(', ')', '{', '}', '>', '<',
              '[', ']', ',', ';', ':', '~', '#', '|', '`']

  EOL='\n'
  EOF=-1

  SKIP_WHITESPACE, READ_STRING, READ_SINGLELINECOMMENT, READ_MULTILINECOMMENT, \
           READ_IDENTIFIER, READ_NUMBER = range(6)
  
  state, string, isfloat, start_lnum, start_chpos = SKIP_WHITESPACE, "", False,\
                                                    None, None
  linenum, chpos, line_chpos = 1, 0, 0
  while chpos <= len(chars):

    # on end, inject an EOF character to flush the final state
    if chpos == len(chars):
      char, char2 = EOF, EOF
    # otherwise, check the character and the next two characters
    else:
      char = chars[chpos]
      char2 = chars[chpos:chpos+2]

    # advance the character position and the line number
    chpos += 1
    if char == EOL:
      linenum += 1
      line_chpos = chpos                # the position of the start of the line

    
    if state == SKIP_WHITESPACE:
      
      if char in SPACECHARS:
        continue
      
      if char == STRINGCHAR:
        state, start_lnum, start_chpos = READ_STRING, linenum, \
                                         chpos - 1 - line_chpos
        continue
      
      if char in NUMBERCHARS or char =='.':
        state, start_lnum, start_chpos = READ_NUMBER, linenum, \
                                         chpos - 1 - line_chpos
        string += char
        if char == '.':
          isfloat = True
        continue
      
      if char in IDENTIFIERCHARS:
        state, start_lnum, start_chpos = READ_IDENTIFIER, linenum, \
                                         chpos - 1 - line_chpos
        string += char
        continue
      
      if char2 == SINGLELINECOMMENTCHARS:
        state, start_lnum, start_chpos = READ_SINGLELINECOMMENT, linenum,\
                                         chpos - 1 - line_chpos
        chpos += 1
        continue
      
      if char2 == OPENMULTILINECOMMENTCHARS:
        state, start_lnum, start_chpos = READ_MULTILINECOMMENT, linenum, \
                                         chpos - 1 - line_chpos
        chpos += 1
        continue
      
      if char2 in SYMBOLS2:
        tokens.append((char2, filename, linenum, chpos - 1 - line_chpos))
        chpos += 1
        continue
      
      if char in SYMBOLS1:
        tokens.append((char, filename, linenum, chpos - 1 - line_chpos))
        continue

      if char == EOF:
        return tokens
      
      illegal_char(filename, linenum, char)
    
    elif state == READ_STRING:
      # handle escaped string character for example:
      # "\"Another quote,\" he screamed!"
      if char2 == ESCAPESTRINGCHAR:
        string += STRINGCHAR
        chpos += 1
        continue
      
      if char == STRINGCHAR:
        tokens.append(('STRINGCONSTANT', filename, start_lnum, start_chpos,
                       string))
        state, string, start_lnum, start_chpos = SKIP_WHITESPACE, "", None, None
        continue

      if char == EOF:
        print >> sys.stderr, "Unexpected EOF in the middle of a string"\
              " in %s:%d" % (filename, linenum)
        sys.exit(1)

      string += char
      continue

    elif state == READ_NUMBER:
      if char2 in ['e+', 'E+', 'e-', 'E-']:
        string += char2
        chpos += 1
        isfloat = True
        continue
      
      if char in ['e', '.']:
        string += char
        isfloat = True
        continue
      
      if char in NUMBERCHARS:
        string += char
        continue
      
      try:
        if isfloat:
          val = float(string)
        else:
          val = int(string)
      
      except ValueError:
        print >> sys.stderr, "Error: illegal number '%s' in %s:%d" \
              % (string, filename, linenum)
        sys.exit(1)
      
      if isfloat:
        tokens.append(('REALCONSTANT', filename, start_lnum, start_chpos, val))
      
      else:
        tokens.append(('INTCONSTANT', filename, start_lnum, start_chpos, val))
      
      state, string, isfloat = SKIP_WHITESPACE, "", False
      start_lnum, start_chpos = None, None
      
      chpos -= 1                        # unget last character
      continue

    elif state == READ_IDENTIFIER:
      if char in IDENTIFIERCHARS or char in NUMBERCHARS:
        string += char
        continue
      
      # check if this is a word in the language
      if string in LANGUAGEWORDS:
        tokens.append((string.upper(), filename, start_lnum, start_chpos,
                       string))

      elif string in BOOLEANWORDS:
        tokens.append(('BOOLCONSTANT', filename, start_lnum, start_chpos,
                       string == "true"))

      # otherwise, it is just a plain identifier
      else:
        tokens.append(('IDENTIFIER', filename, start_lnum, start_chpos, string))

      state, string = SKIP_WHITESPACE, ""
      start_lnum, start_chpos = None, None
      chpos -= 1                        # unget
      continue
    
    elif state == READ_SINGLELINECOMMENT:
      if char == EOL:
        state = SKIP_WHITESPACE
        
      continue

    elif state == READ_MULTILINECOMMENT:
      if char2 == CLOSEMULTILINECOMMENTCHARS:
        state = SKIP_WHITESPACE
        chpos += 1
      
      continue

    else:
      assert(False)                     # illegal state
      
def illegal_char(filename, linenum, ch):
  print >> sys.stderr, "Error: illegal char '%s' in %s:%d" % (ch, filename,
                                                              linenum)
  sys.exit(1)
