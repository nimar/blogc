"""
 Copyright (c) 2012, Nimar S. Arora <nimar.arora@gmail.com>
 All rights reserved.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
     * Redistributions of source code must retain the above copyright
       notice, this list of conditions and the following disclaimer.
     * Redistributions in binary form must reproduce the above copyright
       notice, this list of conditions and the following disclaimer in the
       documentation and/or other materials provided with the distribution.
     * Neither the name of the copyright holders nor the
       names of its contributors may be used to endorse or promote products
       derived from this software without specific prior written permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 THE COPYRIGHT HOLDERS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.
"""
from tokenize import *
from syntax import Parser
from symbol import *

NUMERIC_TYPES = [("Integer",), ("Real",)]

class Expr:
  """
  syntree  : from the syntax tree
  valtype  : value type
  action ; value :
             return_value  ; value expression
             sample_value  ; distribution symbol, argument expressions...
             eval_constant ; constant value
             eval_function ; function symbol, argument expressions...
             eval_argument ; argument number
             eval_operator ; operator name, operand expressions...
  """
  pass

class SemanticParseException (Exception):
  pass

def parse_model_files(context):
  """
  Parsing the model files consists of multiple steps. First we check
  the syntax, then the declarations and finally the expressions. Note
  that parsing only involves building trees, code generation is done in
  a separate step.
  """
  context.errorcnt = 0
  
  parse_syntax(context)
  
  init_symbol_table(context)
  
  parse_type_declarations(context)      # add type names to symbol table

  parse_other_declarations(context)     # add all other names to symbol table

  parse_expressions(context)

  if context.veryverbose:
    print_symbol_table(context)
  
def parse_syntax(context):
  context.parser = Parser()
  
  treelist = []

  # If we hit a syntax error in one file then continue processing the
  # other files for other possible syntax errors. The errorcnt in the context
  # will be incremented though and this function will exit out in the end.
  
  for fname in context.filelist:
    
    tokens = tokenize(context, fname)
    
    try:
      
      tree = context.parser.parse(tokens)
    
    except context.parser.ParseErrors, e:
      for token,expected in e.errors:
        if token[0] == context.parser.EOF:
          print >>sys.stderr, "%s: error: unexpected end of file" % fname
          continue
        
        found = repr(token[0])
        if len(expected) == 1:
          msg = "missing %s (found %s)" % (repr(expected[0]), found)
        else:
          msg1 = "unexpected %s, "% found
          l = sorted([repr(s) for s in expected])
          msg2 = "expected one of "+", ".join(l)
          msg = msg1 + msg2
        token_print_error(context, token, msg)

      continue
      
    if context.veryverbose:
      print_syntax_tree(context, tree)
    
    treelist.append(tree)

  context.syntax_trees = treelist

  if context.errorcnt:
    print >> sys.stderr, "pyblogc: syntax errors encountered"
    sys.exit(1)
  
  elif context.verbose:
    print "parse_syntax: ok"

def parse_type_declarations(context):
  for tree in context.syntax_trees:
    for stmt in tree[1:]:
      if stmt[0] == "typestmt":
        new_symbol(context, (stmt[2][TOK_VALPOS],), "type", None, stmt[2])
  
  if context.errorcnt:
    print >> sys.stderr, "pyblogc: errors in type declarations"
    sys.exit(1)
  
  elif context.verbose:
    print "parse_type_declarations: ok"
        

def parse_other_declarations(context):
  """
  parse declarations other than types
  """
  for tree in context.syntax_trees:
    for stmt in tree[1:]:
      # TODO guarstmt
      # TODO obsstmt -> skolem constants
      if stmt[0] == "fnstmt":
        #                 [1]            [2]    [3]       [4:]
        # fnstmt : (RANDOM | NONRANDOM) type fnnameargs _fnbody ;
        #
        #                 [1]
        # fnnameargs : IDENTIFIER
        #              [3]                   [6]
        #     ( '(' ( type IDENTIFIER ( ',' type IDENTIFIER ) * ) ? ')' ) ?;

        fnname = [stmt[3][1][TOK_VALPOS]]
        # append all the argument types
        for argtype in stmt[3][3::3]:
          fnname.append(parse_type(context, argtype))
        
        fnsym = new_symbol(context, tuple(fnname), "function",
                           parse_type(context, stmt[2]), stmt[3][1])
        
        fnsym.deptype = stmt[1][TOK_VALPOS] # RANDOM or NONRANDOM
        fnsym.deptree = stmt[4:]
  
  if context.errorcnt:
    print >> sys.stderr, "pyblogc: errors in declarations"
    sys.exit(1)
  elif context.verbose:
    print "parse_other_declarations: ok"

def parse_expressions(context):
  for tree in context.syntax_trees:
    for stmt in tree[1:]:
      try:
        if stmt[0] == "fnstmt":
          #                 [1]            [2]    [3]       [4:]
          # fnstmt : (RANDOM | NONRANDOM) type fnnameargs _fnbody ;
          #
          #                 [1]
          # fnnameargs : IDENTIFIER
          #              [3]                   [6]
          #     ( '(' ( type IDENTIFIER ( ',' type IDENTIFIER ) * ) ? ')' ) ?;

          # construct the function name and all the arguments
          fnname = [stmt[3][1][TOK_VALPOS]]
          fnargs = {}
          # append all the argument types
          for argnum in range(1, len(stmt[3]) / 3):
            argtype, argid = stmt[3][argnum*3:argnum*3+2]
            fnname.append(parse_type(context, argtype))
            fnargs[(argid[TOK_VALPOS],)] = (argnum - 1,
                                            parse_type(context, argtype))
          fnname = tuple(fnname)
          
          fnsym = context.symtable[fnname] # better exist
          assert(fnsym.class_ == "function")
          
          # TODO: check dependency type
          
          depexpr = parse_fnbody(context, fnsym, fnargs, stmt[4:])

          fnsym.depexpr = depexpr
          
        # TODO: other statements
        
      except SemanticParseException:
        pass
      
  if context.errorcnt:
    print >> sys.stderr, "pyblogc: errors in expressions"
    sys.exit(1)
  
  elif context.verbose:
    print "parse_expressions: ok"

def parse_fnbody(context, fnsym, fnarguments, deptree):
  # TODO '_multidependency'
  depexpr = parse_singledependency(context, fnarguments, deptree)
  if depexpr.valtype != fnsym.valtype:
    token_print_error(context, deptree[0], "unexpected return type '%s'"
                      " expected '%s'"
                      % (format_symbol_name(depexpr.valtype),
                         format_symbol_name(fnsym.valtype)))
    raise SemanticParseException
  
  return depexpr

def parse_singledependency(context, fnarguments, deptree):
  expr = Expr()
  expr.syntree = deptree
  
  #                     [0]   [1]            [3]
  # _singledependency : '~' IDENTIFIER '(' exprlist ')'
  if deptree[0][0] == "~":
    expr.action = "sample_value"
    distname = deptree[1][TOK_VALPOS]
    parse_exprlist(context, fnarguments, deptree[3])
    # TODO populate expr.value expr.valtype
  
  #                     [0]  [1]
  #                   | '=' expr ;
  elif deptree[0][0] == "=":
    expr.action = "return_value"
    expr.value = parse_expr(context, fnarguments, deptree[1])
    expr.valtype = expr.value.valtype
  
  return expr


def parse_type(context, typeclause):
  assert(typeclause[0] == "type")
  
  if typeclause[1][0] == "IDENTIFIER":
    return (typeclause[1][TOK_VALPOS],)
  else:
    unimp()

def print_syntax_tree(context, tree, indent=0):
  """Print a parse tree to stdout."""
  if not indent:
    print "Syntactical Parse Tree:"
  
  prefix = "  " * indent
  
  # terminals
  if tree[0] in context.parser.terminals:
    print prefix + repr(tree[0:1] + tree[TOK_VALPOS:])

  # non-terminals
  else:
    print prefix + unicode(tree[0])
    for x in tree[1:]:
      print_syntax_tree(context, x, indent+1)

def parse_exprlist(context, localvars, tree):
  #              [1]        [3] ...
  # exprlist : ( expr (',' expr) * ) ? ;
  assert(tree[0] == "exprlist")

  exprs = []
  for subtree in tree[1::2]:
    exprs.append(parse_expr(context, localvars, subtree))
  
  return exprs

def parse_expr(context, localvars, tree):
  """
  returns an Expr object
  """
  expr = Expr()
  expr.syntree = tree
  
  # _expr_const : INTCONSTANT | REALCONSTANT | STRINGCONSTANT | BOOLCONSTANT;
  
  if expr.syntree[0] == 'INTCONSTANT':
    expr.valtype = ('Integer',)
    expr.value = tree[TOK_VALPOS]
    expr.action = "eval_constant"
  
  elif expr.syntree[0] == 'REALCONSTANT':
    expr.valtype = ('Real',)
    expr.value = tree[TOK_VALPOS]
    expr.action = "eval_constant"
  
  elif expr.syntree[0] == 'STRINGCONSTANT':
    expr.valtype = ('String',)
    expr.value = tree[TOK_VALPOS]
    expr.action = "eval_constant"
  
  elif expr.syntree[0] == 'BOOLCONSTANT':
    expr.valtype = ('Boolean',)
    expr.value = tree[TOK_VALPOS]
    expr.action = "eval_constant"
  
  # expr_fncall : IDENTIFIER ( '(' exprlist ')' ) ? ;
  elif expr.syntree[0] == "expr_fncall":
    # parse any arguments
    if len(expr.syntree) > 2:
      fnargexprs = parse_exprlist(context, localvars, expr.syntree[3])
    else:
      fnargexprs = []
                                 
    # construct a name from the function and its argument types
    fnname = [expr.syntree[1][TOK_VALPOS]]
    for argexpr in fnargexprs:
      fnname.append(argexpr.valtype)
    fnname = tuple(fnname)
    
    if len(fnargexprs)==0 and fnname in localvars:
      expr.value, expr.valtype = localvars[fnname]
      expr.action = "eval_argument"
      
    elif fnname in context.symtable:
      fnsym = context.symtable[fnname]
      expr.value = tuple([fnsym] + fnargexprs)
      expr.valtype = fnsym.valtype
      expr.action = "eval_function"
    
    else:
      token_print_error(context, expr.syntree[1], "undefined symbol '%s'"
                        % format_symbol_name(fnname))
      raise SemanticParseException
    
    
  # expr_set_cardinality : '#' ? _setexpr;
  elif expr.syntree[0] == "expr_set_cardinality":
    # TODO
    pass
  
  # expr_list_cardinality : '#' ? _listexpr;
  elif expr.syntree[0] == "expr_list_cardinality":
    # TODO
    pass
  
  # expr_parenthesized : '(' expr ')';
  elif expr.syntree[0] == "expr_parenthesized":
    return parse_expr(context, localvars, tree[2])

  elif expr.syntree[0] == "expr_tuple":
    # TODO
    pass
  
  elif expr.syntree[0] == "expr_matrix_const":
    # TODO
    pass
  
  elif expr.syntree[0] == "expr_vector_const":
    # TODO
    pass

  # expr_unary_op : ('!' | '-') _expr1 ;
  elif expr.syntree[0] == "expr_unary_op":
    oper = tree[1][0]
    operand = parse_expr(context, localvars, tree[2])
    expr.action = "eval_operator"
    expr.value = (oper, operand)
    
    if oper == '!' and operand.valtype != ("Boolean",):
      token_print_error(context, tree[1], "unsupported operand for '!' : '%s'"\
                        % format_symbol_name(operand.valtype))
      raise SemanticParseException
    else:
      expr.valtype = ("Boolean",)

    if oper == '-' and operand.valtype not in NUMERIC_TYPES:
      token_print_error(context, tree[1], "unsupported operand for '-' : '%s'"\
                        % format_symbol_name(operand.valtype))
      raise SemanticParseException
    else:
      expr.valtype = operand.valtype
    
  # expr_multiplication_op : _expr2 ('*' | '/') _expr1 ;
  # expr_addition_op :_expr3 ('+' | '-') _expr2 ;
  elif expr.syntree[0] in ("expr_multiplication_op", "expr_addition_op"):
    oper = tree[2][0]
    oprnd1 = parse_expr(context, localvars, tree[1])
    oprnd2 = parse_expr(context, localvars, tree[3])
    expr.value = (oper, oprnd1, oprnd2)

    if oprnd1.valtype not in NUMERIC_TYPES \
       or oprnd2.valtype not in NUMERIC_TYPES:
      token_print_error(context, tree[2], "unsupported operands for '%s' : "
                        "'%s' and '%s'"
                        % (oper, format_symbol_name(oprnd1.valtype),
                           format_symbol_name(oprnd2.valtype)))
      raise SemanticParseException

    if oprnd1.valtype == oprnd2.valtype:
      expr.valtype = oprnd1.valtype
    else:
      expr.valtype = ("Real",)

  # expr_comparison_op : _expr4 ('>' | '<' | '>=' | '<=' ) _expr3 ;
  # 
  # expr_equality_op : _expr5 ( '=' | '!=' ) _expr4 
  #                  | _expr5 ( '=' | '!=' ) NULL
  #                  | NULL ( '=' | '!=' ) _expr4;
  # 
  # expr : _expr5 | expr_logical_op ;
  # 
  # expr_logical_op : expr ('->' | '|' | '&') _expr5 ;
  # 
  # expr_tuple : '(' expr (',' expr ) + ')' ;
  # 
  # expr_matrix_const : '`' expr_matrix_const (',' expr_matrix_const ) * '`' 
  #                   |  '`' expr_vec_const (',' expr_vec_const ) * '`' ;
  # 
  # expr_vec_const : '`' _expr_const (',' _expr_const) * '`';
  # 
  # expr_fncall : IDENTIFIER ( '(' exprlist ')' ) ? ;
  # 

  else:
    assert(False)
    
  return expr

